class SiteInitializer {
	static initialize (config) {
		this.debugMode = false;
		this.hostname = window.location.hostname;
		
		if (this.hostname === "") {
			this.debugMode = true;
		}
		
		this.validate (config);
		
		console.log (this.config);
		
		// Run the asset resolver
		// AssetManager.initialize()
		
		// Run the Template resolver
		// TemplateManager.initialize();
	}
	
	static validate (config) {
		this.config = {};
		
		if (!config.hasOwnProperty("asset_dir_segs")) {
			console.error ("Page configuration settings must include \"asset_dir_segs\".");
			return;
		} else {
			var val = parseInt(config["asset_dir_segs"]);
			
			if (typeof val !== "integer" || val < 0) {
				console.error ("Page configuration \"asset_dir_segs\" must be a positive integer.");
				return;
			} else {
				this.config["asset_dir_segs"] = val;
			}
		}
		
		// Check if there are any CSS assets to include; if not, skip it
		if (config.hasOwnProperty ("css_include")) {
			// Check that the entry is a propper array
			if (typeof config["css_include"] !== "array") {
				console.error ("Page configuration \"css_include\" must include be a list of CSS asset paths.");
			} else {
				this.config["css_include"] = [];
				
				// Check that each entry is correctly formatted
				var len = config["css_include"].length;
				
				for (var i = 0; i < len; i++) {
					if (
						typeof config["css_include"][i] !== "string" ||
						config["css_include"][i].trim ().length < 1
					) {
						console.error ("CSS asset #" + i + " must be an alphanumeric path name.");
					} else {
						this.config["css_include"].push (config["css_include"][i]);
					}
				}
			}
		}
		
		if (config.hasOwnProperty ("js_include")) {
			// Check that the entry is a propper array
			if (typeof config["js_include"] !== "array") {
				console.error ("Page configuration \"js_include\" must include be a list of Javascript asset paths.");
			} else {
				this.config["js_include"] = [];
				
				// Check that each entry is correctly formatted
				var len = config["js_include"].length;
				
				for (var i = 0; i < len; i++) {
					if (
						typeof config["js_include"][i] !== "string" ||
						config["js_include"][i].trim ().length < 1
					) {
						console.error ("Javascript asset #" + i + " must be an alphanumeric path name.");
					} else {
						this.config["js_include"].push (config["js_include"][i]);
					}
				}
			}
		}
	}
}